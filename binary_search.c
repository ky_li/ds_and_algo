#include<stdio.h>

int check(int mid); //check mid

int binary_search(int q[], int l, int r, int targ)
{
    // |- - - - - - - -|
    //        ^
    // |------|
    // 取左半邊的最右邊模板
    while (l < r)
    {
        int m = l + r + 1 >> 1; //[warning] overflow issue, l 往前可能會導致最右邊沒有檢查到所以加1
        if (check(m)) l = m; //取左半邊的最右邊，所以l往前走且m是true，所以l = m；
        else r = m - 1;
    }
    if (q[l] == targ) return l;

    // |- - - - - - - -|
    //        ^
    //        |--------|
    // 取右半邊的最左邊模板
    while (l < r)
    {
        int m = l + r >> 1; 
        if (check(m)) r = m; //取右半邊的最左邊，所以r往後且m是true，所以r = m；
        else l = m + 1;
    }
    if (q[l] == targ) return l;

}


