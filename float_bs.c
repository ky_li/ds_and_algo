#include<stdio.h>
// 求三次方根
int main()
{
    double n;
    scanf("%lf", &n);
    double l = 0, r = 1e5;
    while (r - l >= 1e-6)
    {
        double m = (l + r) / 2;
        if (m * m * m <= n) l = m;
        else r = m;
    }
    printf("%lf", l);
    return 0;
}