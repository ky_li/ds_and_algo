#include <stdio.h>
#define N 100010
int q[N];

void qs(int l, int r)
{
    if (l >= r) return;
    int i = l - 1, j = r + 1;
    int x = q[l + r >> 1];
    while (i < j)
    {
        while (q[++ i] < x);
        while (q[-- j] > x);
        if (i < j)
        {
            int t = q[i];
            q[i] = q[j];
            q[j] = t;
        }
    }
    qs(l, j);
    qs(j + 1, r);
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i ++) scanf("%d", &q[i]);
    qs(0, n - 1);
    for (int i = 0; i < n; i ++) printf("%d ", q[i]);

    return 0;
}
