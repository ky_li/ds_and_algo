#include <stdio.h>
#define N 100010

int q[N];

int qs(int l, int r, int k)
{
    if (l == r) return q[l];
    int x = q[l + r >> 1];
    int i = l - 1, j = r + 1;
    while (i < j)
    {
        while (q[++ i] < x);
        while (q[-- j] > x);
        if (i < j)
        {
            int t = q[i];
            q[i] = q[j];
            q[j] = t;
        }
    }
    int sl = j - l + 1;
    if (k <= sl) return qs(l, j, k);
    else return qs(j + 1, r, k - sl);
}

int main()
{
    int n, k;
    scanf("%d%d", &n, &k);
    for (int i = 0; i < n; i ++) scanf("%d", &q[i]);
    printf("%d", qs(0, n - 1, k));
    return 0;
}