#include<stdio.h>
#define N 100010

int q[N], t[N];

void mg(int l, int r)
{
    if (l >= r) return;
    int m = l + r >> 1;
    mg(l, m), mg(m + 1, r);
    int i = l, j = m + 1, k = 0;
    while (i <= m && j <= r)
    {
        if (q[i] <= q[j]) t[k ++] = q[i ++];
        else t[k ++] = q[j ++];
    }
    while (i <= m) t[k ++] = q[i ++];
    while (j <= r) t[k ++] = q[j ++];
    for (int i = l, j = 0; i <= r; i ++, j ++) q[i] = t[j];
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i ++) scanf("%d", &q[i]);
    mg(0, n - 1);
    for (int i = 0; i < n; i ++) printf("%d ", q[i]);
    return 0;
}