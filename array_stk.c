#include <stdio.h>
#define N 100010

int stk[N], tt = -1;

int top()
{
    if (tt != -1) return stk[tt];
    else return -1;
}

void pop()
{
    tt --;
}

void push(int x)
{
    stk[++ tt] = x;
}